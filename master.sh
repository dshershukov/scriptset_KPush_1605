echo $(date) "Starting..." | tee -a log 

# need to sum svol_a, svol_b, r_mreturn, r_preturn
# list of fields: IID, Paper, Timestamp, lmpreturn, lpreturn_lags, lmpreturn_ret, lmmreturn, lmreturn_lags, lmmreturn_ret, lmprice, lprice_lags, lmprice_ret, svol_a, svol_b 

# Numbered:
#  1 IID
#  2 Paper
#  3 Timestamp
#  4 lmpreturn
#  5 lpreturn_lags
#  6 lmpreturn_ret
#  7 lmmreturn
#  8 lmreturn_lags
#  9 lmmreturn_ret
# 10 lmprice
# 11 lprice_lags
# 12 lmprice_ret
# 13 svol_a
# 14 svol_b 

# Need to provide a GID (Group IDentificator) and a field for editing for each instance of sum script.

# Temporarily assume that r_mreturn is lmmreturn, similarly for r_preturn

# Step 1. Provide GIDs after field 1. Field structure becomes:

# Numbered:
#  1 IID
#  2 GID
#  3 Paper
#  4 Timestamp
#  5 lmpreturn
#  6 lpreturn_lags
#  7 lmpreturn_ret
#  8 lmmreturn
#  9 lmreturn_lags
# 10 lmmreturn_ret
# 11 lmprice
# 12 lprice_lags
# 13 lmprice_ret
# 14 svol_a
# 15 svol_b 

cat ./raw_files/* | mawk -f './scripts/make_stamp.awk' | tee >(mawk -f './scripts/sum_svol_A.awk' > ./work_files/sum_svol_A) | tee >(mawk -f './scripts/sum_svol_B.awk' > ./work_files/sum_svol_B) | tee >(mawk -f './scripts/sum_lmpreturn_ret.awk' > ./work_files/sum_lmpreturn_ret) | tee >(mawk -f './scripts/sum_lmmreturn_ret.awk' > ./work_files/sum_lmmreturn_ret) > ./work_files/temp_res

echo $(date) "Finshed computations, starting aggregation..." | tee -a log 

join -t $'\t' -a1 -1 1 -2 1 ./work_files/sum_svol_A ./work_files/sum_svol_B | join -t $'\t' -a1 -1 1 -2 1 - ./work_files/sum_lmpreturn_ret | join -t $'\t' -a1 -1 1 -2 1 - ./work_files/sum_lmmreturn_ret | mawk -f './scripts/prepare_for_win.awk' > ./result/result.txt

echo $(date) "Finshed aggregations..." | tee -a log