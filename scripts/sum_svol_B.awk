BEGIN{
	FS="\t"
	OFS="\t"
	
	fid=2;
	fsrc=15;
	
	prev_id="";
	csum="";
}

{
	if (NR == 1) { print "ID", "sum_svol_b"; next; }
	if (NR == 2) { csum=$fsrc; prev_id=$fid; next; }
	if (prev_id == $fid) { if ($fsrc != "") { csum = csum + $fsrc;} } else { print prev_id, csum; csum=$fsrc; prev_id = $fid; }
}

END{
	print prev_id, csum;
}