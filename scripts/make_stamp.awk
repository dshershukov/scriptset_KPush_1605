BEGIN{
	FS="\t"
	OFS="\t"
	RS="\r\n"
	ORS="\n"

	fid=1
}
{
	if (NR==1) {
		$fid=$fid FS "GID"
		print $0
		next
	}
	$fid=$fid FS substr($fid,1,length($fid)-2)
	gsub(FS "NA", FS, $0)
	print($0)
}
